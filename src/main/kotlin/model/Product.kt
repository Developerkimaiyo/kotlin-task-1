package model

data class Product(var name: String, var price: Int, var category:String )

val productList = listOf(
    Product("Rice", 1445 , "Food"),
    Product("Sugar", 1600 , "Food"),
    Product("Confidence", 1000 , "Beauty"),
    Product("Amarula", 2700 , "Drinks"),
    Product("Ultra Clean", 400 , "Household"),
    Product("Kasuku", 500 , "Exercise Books"),
    Product("Fay", 1270 , "Household"),
    Product("Unga", 900 , "Food")
)