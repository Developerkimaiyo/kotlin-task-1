import quiz.*

fun main() {
    val sorting = Sorting()
    val mapping = Mapping()
    val filtering = Filtering()
    val conditions = Conditions()

    println(sorting.getProductsSortedByCashCost())
    print(mapping.getProductsCategories())
    print(filtering.getProductsFromCategory("Household"))
    print(conditions.checkAllProductsCategory("Beauty"))
    print(conditions.hasProductInCategory("Beauty"))
    print(conditions.countProductsFromCategory("Food"))
    print(conditions.findProductFromCategory("Drinks"))

}