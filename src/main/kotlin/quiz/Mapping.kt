package quiz

import model.productList

class Mapping {
    fun getProductsCategories(): Set<String> {

        return productList.map { productItems -> productItems.category }.toSet()
    }
}