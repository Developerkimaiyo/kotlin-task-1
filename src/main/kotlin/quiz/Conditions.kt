package quiz

import model.Product
import model.productList

class Conditions {
    // returning true if all products belong to the same product category and false if not
    fun checkAllProductsCategory(productCategory:String): Boolean {
        val categories = productList.map{ productItems -> productItems.category}
        for (category in categories) {
            if (category != productCategory) return false
        }
        return true
    }


    // returning true if there is at least one product from the product category
    fun hasProductInCategory(productCategory: String ): Boolean {
        val categories = productList.map{ productItems -> productItems.category}
        if(categories.contains(productCategory)) return true
        return false

    }


    //returning the number of occurrence of a product from a given category
    fun countProductsFromCategory(productCategory: String): Int {
        val categories = productList.map{ productItems -> productItems.category}
        return  categories.count{ it == productCategory }
    }

   // returning a product in the category "Drinks" or null if there is none
    fun findProductFromCategory(productCategory: String): Product? {
      return productList.find { it.category == productCategory }

    }
}