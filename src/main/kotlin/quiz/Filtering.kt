package quiz

import model.Product
import model.productList

class Filtering {
    fun getProductsFromCategory(category:String): List<Product>{

        return productList.filter { it.category == category }
    }
}